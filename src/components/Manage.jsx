import React, { useState } from "react";
import style from "../assets/css/Manage.module.css";
import cryptoJs from "crypto-js";
import { Link } from "react-router-dom";

const Manage = () => {
    const [masterPassword, setMasterPassword] = useState("");
    const [display, setDisplay] = useState("");

    const getPass = localStorage.getItem("password");
    const handleSubmit = (e) => {
        e.preventDefault();
        const localData = localStorage.getItem("data");
        if (!localData) {
            alert("Please join first");
        }

        const decrypted = JSON.parse(
            cryptoJs.AES.decrypt(localData, "Iy3yEnkxT4f0ytH2tQjC").toString(
                cryptoJs.enc.Utf8,
            ),
        );

        if (masterPassword !== decrypted.masterPassword) {
            alert("Invalid Password");
        } else {
            const decPass = JSON.parse(
                cryptoJs.AES.decrypt(getPass, "Iy3yEnkxT4f0ytH2tQjC").toString(
                    cryptoJs.enc.Utf8,
                ),
            );
            setDisplay(decPass);
        }
    };

    return (
        <div className={style.manageFormContainer}>
            <form className={style.manageForm}>
                <input type="text" placeholder="Username" />
                <input
                    type="password"
                    placeholder="Master Password"
                    value={masterPassword}
                    onChange={(e) => setMasterPassword(e.target.value)}
                />
                <button
                    disabled={masterPassword === "" ? true : false}
                    onClick={handleSubmit}>
                    Submit
                </button>

                {display ? (
                    <>
                        <h3>Application: {display?.app}</h3>
                        <h3>Password: {display?.pass}</h3>
                    </>
                ) : (
                    <div className={style.link}>
                        <p>
                            Haven't joined yet? <Link to="/join">Join Now</Link>
                        </p>
                    </div>
                )}
            </form>
        </div>
    );
};

export default Manage;
