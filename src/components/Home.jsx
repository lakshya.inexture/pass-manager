import React from "react";
import { Link } from "react-router-dom";
import style from "../assets/css/Home.module.css";

const Home = () => {
    return (
        <div className={style.home_wrapper}>
            <h1>OnePassword</h1>
            <h3></h3>
            <div className={style.home}>
                <Link to="/join">
                    <p className={style.box}>Save Passwords</p>
                </Link>

                <Link to="/manage">
                    <p className={style.box}>Access</p>
                </Link>
            </div>
        </div>
    );
};

export default Home;
