import React, { useState } from "react";
import style from "../assets/css/Join.module.css";
import cryptoJs from "crypto-js";
import { Link } from "react-router-dom";

const Join = () => {
    const initialState = { username: "", masterPassword: "", pass: "" };
    const initState = { app: "", pass: "" };
    const [data, setData] = useState(initialState);
    const [appData, setAppData] = useState(initState);

    const handleSubmit = (e) => {
        e.preventDefault();
        const encData = cryptoJs.AES.encrypt(
            JSON.stringify(data),
            "Iy3yEnkxT4f0ytH2tQjC",
        ).toString();
        const encPass = cryptoJs.AES.encrypt(
            JSON.stringify(appData),
            "Iy3yEnkxT4f0ytH2tQjC",
        ).toString();
        localStorage.setItem("data", encData);
        localStorage.setItem("password", encPass);
        setData(initialState);
        setAppData(initState);
    };

    return (
        <div className={style.joinFormContainer}>
            <form className={style.joinForm}>
                <h3>Create a username</h3>
                <input
                    type="text"
                    placeholder="username"
                    value={data.username}
                    onChange={(e) =>
                        setData({ ...data, username: e.target.value })
                    }
                />

                <h3>Put in your email</h3>
                <input type="text" name="email" placeholder="Email" />

                <h3>
                    Save your passwords by putting in the name of the
                    application, and the password you want to save:
                </h3>
                <input
                    type="text"
                    placeholder="Application"
                    value={appData.app}
                    onChange={(e) =>
                        setAppData({ ...appData, app: e.target.value })
                    }
                />
                <input
                    type="password"
                    placeholder="Password"
                    value={appData.pass}
                    onChange={(e) =>
                        setAppData({ ...appData, pass: e.target.value })
                    }
                />

                <h3>Create your master password</h3>
                <input
                    type="password"
                    placeholder="Master Password"
                    value={data.masterPassword}
                    onChange={(e) =>
                        setData({ ...data, masterPassword: e.target.value })
                    }
                />
                <button
                    disabled={
                        data.username === "" || data.masterPassword === ""
                            ? true
                            : false
                    }
                    onClick={handleSubmit}>
                    Submit
                </button>
                <div className={style.link}>
                    <p>
                        Already saved? <Link to="/manage">Access</Link>
                    </p>
                </div>
            </form>
        </div>
    );
};

export default Join;
