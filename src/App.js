import { useRoutes } from "react-router-dom";
import Join from "./components/Join";
import Manage from "./components/Manage";
import Home from "./components/Home";
import "./app.css";

function App() {
    const routes = useRoutes([
        { path: "/", element: <Home /> },
        { path: "/join", element: <Join /> },
        { path: "/Manage", element: <Manage /> },
    ]);
    return routes;
}

export default App;
